<?php

namespace app\common;

use Yii;

/**
 * Created by PhpStorm.
 * User: pavel.chermyanin
 * Date: 22/05/2018
 * Time: 11:08
 */
class OutputFormatter
{
    public function publishedDate($publishedDate){
        $formatter = Yii::$app->formatter;
        return $formatter->asDate($publishedDate, 'dd.MM.yyyy');
    }
}