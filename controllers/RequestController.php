<?php

namespace app\controllers;

use app\models\CalculationRequest;
use app\models\CalculationRequestSearch;
use PHPUnit\Framework\Exception;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * RequestController implements the CRUD actions for CalculationRequest model.
 */
class RequestController extends Controller
{

    public $layout = 'admin';
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'create' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),

                'rules' => [
                    ['allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all CalculationRequest models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CalculationRequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CalculationRequest model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CalculationRequest model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        try {
            $model = new CalculationRequest();
            if (Yii::$app->request->post()["CalculationRequest"]==null) {
                throw new Exception("invalid input data");
            }
            $phone = Yii::$app->request->post()['CalculationRequest']['phone'];
            $fModel = CalculationRequest::find()->where("phone like :phone", [':phone' => '%' . substr(trim($phone), -10)])->one();
            if ($fModel != null) {
                $fModel->delete();
            }
            if ($model->load(Yii::$app->request->post()) && $model->save()) {


                $userArray = ["result" => 'ok'];
                return $this->asJson($userArray);
            }
            $userArray = ["result" => 'fail'];
            return $this->asJson($userArray);
        } catch (Exception $e) {

            $userArray = ["result" => 'fail', "message" => $e->getMessage()];
            return $this->asJson($userArray);
        }

    }

    /**
     * Updates an existing CalculationRequest model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CalculationRequest model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CalculationRequest model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CalculationRequest the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CalculationRequest::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
