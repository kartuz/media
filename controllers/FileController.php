<?php

namespace app\controllers;

use Yii;
use yii\helpers\Json;
use yii\web\Response;
use yii\filters\AccessControl;


class FileController extends \yii\web\Controller
{

    public $enableCsrfValidation = false;

    public function actions()
    {
        return [
            'upload' => [
                'class' => 'troy\ImageUpload\UploadAction',
                'successCallback' => [$this, 'successCallback']
                // 'beforeStoreCallBack' => [$this,'beforeStoreCallback']
            ],

        ];
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'=>['upload'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['upload'],
                        'roles' => ['@'],
                    ],
                ],
            ]];
    }

    public function successCallback($store, $file)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $res = $store->getFileAttributes();
        $res["location"] = $store->getFileAttributes()['store'];
        Yii::$app->response->content = Json::encode($res);
        echo Yii::$app->response->content;
        die;
        //return Yii::$app->response;
    }

    public function beforeStoreCallback($file)
    {
var_dump($file);
        die;
    }

    /**
     * @return Action
     */
    public function actionShow($url)
    {

        $fp = fopen($url, 'rb');

// send the right headers
        header("Content-Type: image/png");
        header("Content-Length: " . filesize($url));

// dump the picture and stop the script
        fpassthru($fp);
        exit;
    }

}
