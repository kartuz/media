<?php

namespace app\controllers;

use app\models\CallRequest;
use app\models\CallRequestSearch;
use Exception;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * CallController implements the CRUD actions for CallRequest model.
 */
class CallController extends Controller
{
    public $layout = 'admin';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['update', 'delete', 'index'],
                'rules' => [

                    [
                        'allow' => true,
                        'actions' => ['update', 'delete', 'index'],
                        'roles' => ['@']
                    ]
                ],
            ],
        ];
    }

    /**
     * Lists all CallRequest models.
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new CallRequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new CallRequest model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        try {
            $model = new CallRequest();

            if (Yii::$app->request->post()["CallRequest"] == null) {
                throw new Exception("invalid input data");
            }
            $phone = Yii::$app->request->post()['CallRequest']['phone'];
            $fModel = CallRequest::find()->where("phone like :phone", [':phone' => '%' . substr(trim($phone), -10)])->one();
            if ($fModel != null) {
                $fModel->delete();
            }
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                $userArray = ["result" => 'ok'];
                return $this->asJson($userArray);
            }
            $userArray = ["result" => 'fail'];
            return $this->asJson($userArray);
        } catch (Exception $e) {

            $userArray = ["result" => 'fail', "message" => $e->getMessage()];
            return $this->asJson($userArray);
        }
    }

    /**
     * Updates an existing CallRequest model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CallRequest model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CallRequest model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CallRequest the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CallRequest::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
