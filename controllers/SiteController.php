<?php

namespace app\controllers;

use app\models\CalculationRequest;
use app\models\CallRequest;
use app\models\CarouselGroup;
use app\models\ContactForm;
use app\models\LoginForm;
use app\models\MenuItem;
use app\models\RequestCalculationForm;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {

        $mainCarousel = CarouselGroup::findOne(['main' => 1]);
        $carousels = CarouselGroup::findAll(['main' => 0]);
        $items = MenuItem::find()->all();
        $rForm = new CalculationRequest();
        return $this->render('index', ['main' => $mainCarousel, 'rFrom' => $rForm, 'carousels' => $carousels, 'menuItems' => $items]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->renderPartial('login', [
            'model' => $model, 'layout' => 'admin'
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * @return
     */
    public static function getCallForm()
    {
        return new CallRequest();
    }

    public static function getRequestForm()
    {
        return new CalculationRequest();
    }


    public function getMenu()
    {
        $menuItems = [];
        $items = MenuItem::find()->all();
        foreach ($items as $row) {
            $menuElem = ['label' => $row->title, 'url' => ['/' . $row->alias], 'active' => mb_strpos(Url::current(), $row->alias)];
            $menuItems[] = $menuElem;
        }
        return $menuItems;
    }

    public function actionPage($alias)
    {
        $item = MenuItem::findOne(['alias' => $alias]);

        return $this->render('_page', ['model' => $item]);
    }

    public function actionRequest()
    {
        return 'request';
    }
}
