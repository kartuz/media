<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%iet_about}}".
 *
 * @property int $id
 * @property string $title
 * @property string $lead
 * @property string $content
 */
class About extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%iet_about}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lead', 'content'], 'string'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'lead' => 'Часть текста до списка команды',
            'content' => 'Часть текста после списка',
        ];
    }
}
