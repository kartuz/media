<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%iet_team}}".
 *
 * @property int $id
 * @property string $img_url
 * @property string $name
 * @property string $role
 */
class Team extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%iet_team}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['img_url'], 'string', 'max' => 255],
            [['name', 'role'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'img_url' => 'Фотография',
            'name' => 'Имя',
            'role' => 'Должность',
        ];
    }
}
