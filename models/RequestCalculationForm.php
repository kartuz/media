<?php
/**
 * Created by PhpStorm.
 * User: pavel.chermyanin
 * Date: 22/08/2018
 * Time: 22:21
 */

namespace app\models;


class RequestCalculationForm extends Model
{
    private $name;
    private $phone;
    private $email;
    private $type;
    private $width;
    private $height;
    private $message;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['phone', 'name'], 'required'],
            // rememberMe must be a boolean value
            ['email', 'email'],
            // password is validated by validatePassword()
            [['type', 'message'], 'string'],
            ['width', 'height', 'integer'],
        ];
    }
    
}