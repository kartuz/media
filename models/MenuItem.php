<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%iet_articles}}".
 *
 * @property int $id
 * @property string $main_img_url
 * @property string $icon_img_url
 * @property string $title
 * @property string $big_title
 * @property string $lead
 * @property string $snipet
 * @property string $alias
 * @property string $block_title
 */
class MenuItem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%md_menu_items}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lead'], 'string'],
            [['main_img_url', 'snippet', 'block_title', 'icon_img_url'], 'string', 'max' => 255],
            [['big_title', 'title'], 'string', 'max' => 100],
            [['alias'], 'string', 'max' => 200],
        ];
    }

    public function getBlocks()
    {
        return $this->hasMany(PageBlock::class, ['menu_item_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'icon_img_url' => 'Иконка для меню',
            'title' => 'Заголовок',
            'big_title' => 'Описание для меню',
            'alias' => 'Алиас для адресной строки',
            'main_img_url' => 'Основное изображение',
            'lead' => 'Аннотация',
            'snipet' => 'Текст для сниппета',
            'block_title' => 'Подзаголовок блоков'
        ];
    }

    public function beforeSave($insert)
    {
        if (empty($this->alias))
            $this->alias = $this->transliterate($this->title);
        if (!parent::beforeSave($insert))
            return false;
        return true;
    }

    public function transliterate($textcyr = null)
    {
        $textcyr = mb_strtolower($textcyr);

        $cyr = array(
            'ё', 'ж', 'х', 'ц', 'ч', 'щ', 'ш', 'ъ', 'э', 'ю', 'я', 'ы', 'а', 'б', 'в', 'г', 'д', 'е', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'ь', ' ');
        $lat = array(
            'yo', 'zh', 'kh', 'ts', 'ch', 'shh', 'sh', '``', 'eh', 'yu', 'ya', 'y', 'a', 'b', 'v', 'g', 'd', 'e', 'z', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', '', '_');
        return str_replace($cyr, $lat, $textcyr);
    }

    public static function getMenuItems($active)
    {
        $menuItems = [];
        $items = MenuItem::findAll();
        foreach ($items as $row) {
            $menuElem = ['label' => $row->title, 'url' => ['/' . $row->alias], 'active' => !mb_strpos($active, $row->alias)];
            $items[] = $menuElem;
        }
    }
}
