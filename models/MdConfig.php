<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%md_config}}".
 *
 * @property int $id
 * @property string $name
 * @property string $value
 */
class MdConfig extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%md_config}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 100],
            [['value'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Параметр',
            'value' => 'Значение',
        ];
    }

    private static $labels = ['from_mail' => 'Адрес с которого будут отправлятся сообщения',
        'call_request_mail' => 'Адрес для оповещения о запросе звонка',
        'calculation_request_mail' => 'Адрес для оповещения о запросе расчета',
        'presentation_file' => 'Url файла с презентацией',
        'site_admin' => 'Почта администратора сайта'];

    public function editUrl()
    {
        return \yii\helpers\Html::a($this->getLabel(), '/config/update/' . $this->id);
    }

    public function getLabel()
    {
        return (isset(self::$labels[$this->name])) ? self::$labels[$this->name] : '';
    }
    public static function presFile()
    {
        return self::find()->where("name=:name", ['name' => 'presentation_file'])->one()->value;
    }
    public static function presFileId()
    {
        return self::find()->where("name=:name", ['name' => 'presentation_file'])->one()->id;
    }

    public static function fromEMail()
    {
        return self::find()->where("name=:name", ['name' => 'from_mail'])->one()->value;
    }

    public static function callEMail()
    {
        return self::find()->where("name=:name", ['name' => 'call_request_mail'])->one()->value;
    }

    public static function adminEMail()
    {
        return self::find()->where("name=:name", ['name' => 'site_admin'])->one()->value;
    }

    public static function requestEMail()
    {
        return self::find()->where("name=:name", ['name' => 'calculation_request_mail'])->one()->value;
    }

    public static function sendCallMail()
    {
        return self::find()->where("name=:name", ['name' => 'call_request_mail'])->one() != null;
    }

    public static function sendCalculationMail()
    {
        return self::find()->where("name=:name", ['name' => 'calculation_request_mail'])->one() != null;
    }
}
