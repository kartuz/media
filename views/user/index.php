<?php

use kartik\icons\Icon;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-db-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить пользователя', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'email:email',
            //'authKey',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}',
                'visible' => true,
                'buttons' => [

                    'update' => function ($url, $model) {
                        return Html::a(Icon::show('pencil'), $url, [
                            'title' => Yii::t('app', 'редактировать'),
                            'class' => "btn btn-link edit"
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a(Icon::show('trashcan'), $url, [
                            'title' => Yii::t('app', ' удалить'),
                            'class' => "btn btn-link delete",
                            'data' => [
                                'confirm' => 'Вы уверены что хотите удалить?',
                                'method' => 'post',
                            ],
                        ]);
                    }

                ],
            ],
        ],
    ]); ?>
</div>
