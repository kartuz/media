<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UserDB */

$this->title = 'Добавление нового пользователя';
$this->params['breadcrumbs'][] = ['label' => 'User Dbs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-db-create card">

    <h1><?= Html::encode($this->title) ?></h1>

    <p> Для входа в систему используются сочетание email|пароль</p>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
