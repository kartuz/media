<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\widgets\Menu;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\assets\FontAsset;

use kartik\icons\Icon;

AppAsset::register($this);
FontAsset::register($this);
Icon::map($this);
//Icon::map($this, Icon::FA);

?>
<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <?= Html::csrfMetaTags(); ?>
    <title><?= Html::encode($this->title) ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>
<div class="" id="page">

    <div id="header">
        <div id="logo"><a href="/" class="site-name">&laquo;3 ART&raquo;</a> <b>Администрирование</b></div>
    </div><!-- header -->


    <div id="mainmenu">
        <?php
        $name = (!Yii::$app->user->isGuest) ? Yii::$app->user->identity->username : "guest";
        echo Menu::widget([
            'items' => array(
                array('label' => 'Главная страница', 'url' => array('/carousel/index')),
                array('label' => 'Страницы', 'url' => array('/page/admin')),
                array('label' => 'Заказанные звонки', 'url' => array('/call/index')),
                array('label' => 'Запрос на расчет', 'url' => array('/request/index')),
                array('label' => 'Параметры конфигурации', 'url' => array('/config/index')),
                array('label' => 'Пользователи', 'url' => array('/user/index')),
                array('label' => 'Вход', 'url' => array('/site/login'), 'visible' => Yii::$app->user->isGuest),
                array('label' => 'Выход (' . $name . ')', 'url' => array('/site/logout'), 'visible' => !Yii::$app->user->isGuest)
            )]);
        ?>
    </div><!-- mainmenu -->
    <div class="container">
        <?= $content ?>
    </div>
    <div class="clear"></div>
    <div id="footer">
        <p>&copy; <?php echo date('Y'); ?> «3Art» </p>


    </div><!-- footer -->
</div>

<?php $this->endBody() ?>

</body>
</html>

<?php $this->endPage() ?>

