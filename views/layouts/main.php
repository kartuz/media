<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\AppAsset;
use app\assets\FontAsset;
use app\controllers\SiteController;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\widgets\Menu;

AppAsset::register($this);
FontAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php echo (isset($this->params['og'])) ? $this->params['og'] : ''; ?>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<?php if (!Yii::$app->user->isGuest) : ?>
    <div class="cms-link">
        <?= Html::a('Редактировать', $this->params['editlink']) ?>
        <?= Html::a('Выйти', ['site/logout']) ?>
    </div>
<?php else: ?>
    <div class="square">
        <?= Html::a('', ['/carousel/index']) ?>
    </div>
<?php endif; ?>
<nav class="navbar navigation-clean-button navbar-default">
    <div class="wrapper">
        <div class="navbar-header">

            <?php if ($this->context->route <> 'article/index'): ?>
                <a class="navbar-brand" href="/">
                    <img class="logo" src="img/logo.svg">
                </a>
            <?php else: ?>
                <span class="navbar-brand">
                 <img class="logo" src="img/logo.svg">
                </span>
            <?php endif; ?>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navcol"
                    aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="navcol">
            <?= Menu::widget([
                'items' => Yii::$app->controller->getMenu(),
                'options' => ['class' => 'nav navbar-nav'],
                'linkTemplate' => '<a href="{url}">{label}</a>'

            ]); ?>
            <div class="contact-area navbar-text action hidden-xs">
                <span class="hidden-sm">Казань, </span><span>+7 963121201</span>
                <a class='button button-outline-red  modal-btn' href="" data-modal="#callModal">Заказать
                    звонок</a>
            </div>
            <div class="row visible-xs sm-contact">

                <p class="phone">+7 963121201</p>

                <div class="col-md-6 col-12">
                    <p> Казань, ул. Салиха Сайдашева, 32 <br/> +7 929 722 22 27 <span style="display: block"><a
                                href="mailto:led@3art.su">led@3art.su</a></span></p>
                </div>
                <div class="col-md-6 col-12">
                    <p>Работаем в будни с 9 до 18 без обеда <br/>суббота с 10 до 15 <br/>воскресенье — выходной</p>
                </div>
                <a class='button button-outline-red  modal-btn' href="" data-modal="#callModal">Заказать звонок</a>
            </div>
        </div>
    </div>
</nav>
<?= $content ?>

<footer>
    <div class="wrapper">

        <div class="left-column">

            <div class="contact-area">
                <span>+7 963121201</span>
                <a class='hidden-sm hidden-xs button button-outline-red  modal-btn' href="" data-modal="#callModal">Заказать
                    звонок</a>
            </div>
            <div class="row">
                <div class="col-md-6 col-12">

                    <p> Казань, ул. Салиха Сайдашева, 32 <br/> +7 929 722 22 27 <span style="display: block"><a
                                href="mailto:led@3art.su">led@3art.su</a></span></p>
                </div>
                <div class="col-md-6 col-12">
                    <p>Работаем в будни с 9 до 18 без обеда <br/>суббота с 10 до 15 <br/>воскресенье — выходной</p>
                </div>
            </div>
        </div>
        <div class="right-column">
            <ul class="social">
                <li><a href=""><img src="img/Facebook.svg"></a></li>
                <li><a href=""><img src="img/Instagram.svg"></a></li>
                <li><a href=""><img src="img/VK.svg"></a></li>
            </ul>
            <div class="credits">
                <p><a href="">Сделано в студии Матрёшка</a> <br/>&copy;3art, 2007—2018</p>
            </div>
        </div>

        <div class="clear"></div>
    </div>
</footer>

<div class="modal fade" id="requestModal" tabindex="-1" role="dialog" aria-labelledby="requestModal"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <?php $form = ActiveForm::begin(['action' => ['/request/create'], 'options' => ['id' => 'requestModalForm', 'class' => 'mform', 'data-success' => "#successRequestModal", 'data-error' => '#errorModal', 'data-method' => "post", 'method' => 'post']]); ?>
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title" id="exampleModalLabel">Заявка на точный расчёт</h2>
            </div>
            <div class="modal-body">
                <?= $form->field(SiteController::getRequestForm(), 'name')->textInput(['maxlength' => true]) ?>
                <?= $form->field(SiteController::getRequestForm(), 'width')->hiddenInput()->label(false) ?>
                <?= $form->field(SiteController::getRequestForm(), 'height')->hiddenInput()->label(false) ?>
                <?= $form->field(SiteController::getRequestForm(), 'type')->hiddenInput()->label(false) ?>
                <?= $form->field(SiteController::getRequestForm(), 'step')->hiddenInput()->label(false) ?>
                <?= $form->field(SiteController::getRequestForm(), 'phone')->textInput(['maxlength' => true]) ?>
                <?= $form->field(SiteController::getRequestForm(), 'email')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="modal-footer">
                <?= Html::submitButton('Отправить', ['class' => 'button button-big']) ?>
                <button type="button" class="button  button-cancel" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

<div class="modal fade " tabindex="-1" role="dialog" id="successRequestModal"
     aria-labelledby="mySmallModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">

                <h2 class="modal-title" id="exampleModalLabel">Заявка отправлена!</h2>
            </div>
            <div class="modal-body">
                <p>Мы произведем расчёты и свяжемся с вами по указанному телфону. </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="button  button-cancel" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="callModal" tabindex="-1" role="dialog" aria-labelledby="callModal"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <?php $form = ActiveForm::begin(['action' => ['/call/create'], 'options' => ['id' => 'callModalForm', 'class' => 'mform', 'data-success' => "#successModal", 'data-error' => "#errorModal", 'data-method' => "post", 'method' => 'post']]); ?>
        <div class="modal-content">
            <div class="modal-header">

                <h2 class="modal-title" id="exampleModalLabel">Заказать расчет</h2>
            </div>
            <div class="modal-body">
                <?= $form->field(SiteController::getCallForm(), 'name')->textInput(['maxlength' => true]) ?>
                <?= $form->field(SiteController::getCallForm(), 'phone')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="modal-footer">
                <?= Html::submitButton('Отправить', ['class' => 'button button-big']) ?>
                <button type="button" class="button  button-cancel" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

<div class="modal fade " tabindex="-1" role="dialog" id="successModal"
     aria-labelledby="mySmallModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">

                <h2 class="modal-title" id="exampleModalLabel">Заявка отправлена!</h2>
            </div>
            <div class="modal-body">
                <p>Мы скоро перезвоним вам. </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="button  button-cancel" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade error" tabindex="-1" role="dialog" id="errorModal"
     aria-labelledby="mySmallModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="modal-title" id="exampleModalLabel">Произошла ошибка!</h2>
            </div>
            <div class="modal-body">
                <p>Попробуйте еще раз позднее. </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="button  button-cancel" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
<?php
$this->registerJs(
    " 
    $('.modal-btn').on('click', function(e){
    e.preventDefault();
    var modalID =$(e.target).data('modal');
   $(modalID).modal('toggle');
    });
    
    $('.modal-dialog form.mform').on('beforeSubmit', function(e){
     e.preventDefault();
     console.log($(e.target).serializeArray());
     $(e.target).parent().parent().modal('hide');
     $.post({
       url: $(e.target).attr('action'), 
       dataType: 'json',
       data: $(e.target).serializeArray(),
       success: function(data) {
         
          $($(e.target).data('success')).modal('show');
          console.log(data);
       },
     error: function(err){
     $($(e.target).data('error')).modal('show');
 
       }
    });
       return false;
    });    ",
    View::POS_READY,
    'modal-form-handler'
);
?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
