<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $slider \app\models\CarouselGroup */
/* @var $model app\models\CarouselBlock */

$this->title = 'Добавление слайда ';
$this->title .= ($slider->main == 1) ? 'в главную карусель' : 'в карусель #'.$slider->ord;
?>
<div class="carousel-block-create card">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model, 'slider' => $slider
    ]) ?>

</div>
