<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CarouselBlock */

$this->title = 'Изменение слайда: #' . $model->ord;
if($model->group->main){
    $this->title = "Основная карусель. ".$this->title;
}else{
    $this->title = "Карусель #".$model->group->ord.". ".$this->title;
}
?>
<div class="carousel-block-update card">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model, 'slider'=>$model->group
    ]) ?>

</div>
