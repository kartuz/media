<?php

use dosamigos\tinymce\TinyMce;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CarouselBlock */
/* @var $slider app\models\CarouselGroup */
/* @var $form yii\widgets\ActiveForm */
?>
<?php if (!$slider->main) { ?>
    <div class="card portlet-content">

        <h2><?= $slider->header ?></h2>
        <?= $slider->content ?>
    </div>
<?php } ?>

<?php $form = ActiveForm::begin(); ?>
<div class="photo article-photo" id="photoMain">
    <?php
    $h = 0;

    if (!empty($model->img_url)) :
        echo Html::img($model->img_url, ['id' => "main_img"]);
        $h = 1;
    endif; ?>
    <div class="inner-dot">
        <a id="imageUpload" href="javascript:;">load</a>
        <img src="" id="img" style="display: none"/>
        <?= \troy\ImageUpload\ImageUpload::widget(
            [
                'targetId' => 'imageUpload',//html dom id
                'config' => [
                    'name' => 'file',
                    'action' => Yii::$app->getUrlManager()->createUrl(['file/upload']),

                    'onComplete' => new JsExpression("function(fileName, responseJSON){
            
                var json = $.parseJSON(responseJSON);
                 $('#img').attr('src', '/'+json.store);
                 $('#carouselblock-img_url').val('/'+json.store);
                 $('#img').show();
                 $('#imageUpload').hide();
                 
                 }")
                ]
            ]
        ); ?>


    </div>
    <div class="delete-hover">
        <?= Html::a('<span class="glyphicon glyphicon-trash"></span>', null, ['class' => 'delete', 'title' => 'Удалить изображение']); ?>

    </div>
    <?php
    $this->registerJs(
        "var hideloadMain = " . $h . "
                if(hideloadMain){
                    $('#photoMain .inner-dot').hide();
                    $('#photoMain .delete-hover').show();
                }
                
                $('#photoMain .delete').on('click', function() { 
                        $('#carouselblock-img_url').val('');
                        $('#photoMain .inner-dot').show();
                        $('#imageUpload').show();
                        $('#img').hide();
                        $('#photoMain .delete-hover').hide();
                    });",
        View::POS_READY,
        'my-button-handler-main'
    );
    ?>
</div>

<?php if (isset($slider) && $slider->main == 1) {
    echo $form->field($model, 'header')->textInput(['maxlength' => true]);
    echo $form->field($model, 'content')->widget(TinyMce::class, [
        'options' => ['rows' => 14],
        'language' => 'ru',
        'clientOptions' => [
            'content_css' => ['//' . Yii::$app->request->baseUrl . '/css/mce.css'],
            'plugins' => [
                "advlist autolink lists link charmap anchor",
                "code fullscreen",
                "contextmenu"
            ],
            'toolbar' => "undo redo | styleselect | bold italic | bullist numlist ",
            'automatic_uploads' => false,
        ]
    ]);
} ?>
<?= $form->field($model, 'img_url')->hiddenInput(['maxlength' => true])->label(false); ?>
<?= $form->field($model, 'group_id')->hiddenInput(['value' => $slider->id])->label(false); ?>

<div class="form-group">
    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
</div>

<?php ActiveForm::end(); ?>
