<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Команда';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="team-index card">

    <h1><?= Html::encode($this->title) ?></h1>

    <ul class="team team-edit">
        <?php
        foreach ($members as $member): ?>
            <li>
                <div class="photo">

                    <?= Html::img($member->img_url); ?>

                </div>
                <div class="caption">

                    <p class="name">
                        <?php echo $member->name; ?>
                    </p>
                    <p class="role">
                        <?php echo $member->role; ?>

                    </p>
                </div>
                <div class="delete-hover">
                        <?= Html::a('<span class="glyphicon glyphicon-trash"></span>', ['team/delete', 'id'=>$member->id], [
                            'title' => 'Удалить из команды',
                            'class' => "delete",
                            'data' => [
                                'confirm' => 'Вы уверены что хотите удалить '.$member->name.' из команды ?',
                                'method' => 'post',
                            ],
                        ]) ?>




                </div>

            </li>
        <?php endforeach; ?>
        <li>
            <div class="photo">
                <div class="inner-dot">

                    <?= Html::a("Добавить", ['/team/create', 'back' => Yii::$app->request->url]); ?>
                </div>
            </div>
        </li>
    </ul>

</div>
