<?php

use yii\helpers\Html;


use yii\widgets\ActiveForm;
use yii\web\JsExpression;


/* @var $this yii\web\View */
/* @var $model app\models\Team */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="team-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'img_url')->hiddenInput(['maxlength' => true])->label(false); ?>
    <div class="photo">
        <?php
        if ($model->img_url != null) :
            echo Html::img($model->img_url);
        else: ?>
            <div class="inner-dot">
                <a id="imageUpload" href="javascript:;">load</a>
                <img src="" id="img" style="display: none"/>
                <?= \troy\ImageUpload\ImageUpload::widget(
                    [
                        'targetId' => 'imageUpload',//html dom id
                        'config' => [
                            'name' => 'file',
                            'action' => Yii::$app->getUrlManager()->createUrl(['file/upload']),

                            'onComplete' => new JsExpression("function(fileName, responseJSON){
            
                var json = $.parseJSON(responseJSON);
                 $('#img').attr('src', '/'+json.store);
                 $('#team-img_url').val('/'+json.store);
                 $('#img').show();
                 $('#imageUpload').hide();
                 
                 }")
                        ]
                    ]
                ); ?>

            </div>
        <?php endif; ?>
    </div>


    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'role')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
