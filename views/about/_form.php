<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;

/* @var $this yii\web\View */
/* @var $model app\models\About */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="about-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'lead')->widget(TinyMce::class, [
        'options' => ['rows' => 15],
        'language' => 'ru',
        'clientOptions' => [
            'content_css' => ['//'.Yii::$app->request->baseUrl.'/css/mce.css'],

            'plugins' => [
                "advlist autolink lists",
                "code ",
                "contextmenu paste"
            ],
            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | link image | code",


        ]
    ]); ?>
    <div class="form-group">
        <label><?= Html::a('Команда', ['/team']) ?></label>
        <ul class="team team-edit">
            <?php
            foreach ($members as $member): ?>
                <li>
                    <div class="photo">
                        <?= Html::img($member->img_url); ?>
                    </div>
                    <div class="caption">

                        <p class="name">
                            <?php echo $member->name; ?>
                        </p>
                        <p class="role">
                            <?php echo $member->role; ?>

                        </p>
                    </div>
                </li>
            <?php endforeach; ?>
            <li>
                <div class="photo">
                    <div class="inner-dot">

                        <?= Html::a("Добавить", ['/team/create', 'back' => Yii::$app->request->url]); ?>

                    </div>
                </div>
            </li>
        </ul>


    </div>

    <?= $form->field($model, 'content')->widget(TinyMce::class, [
        'options' => ['rows' => 15],
        'language' => 'ru',
        'clientOptions' => [
            'content_css' => ['//'.Yii::$app->request->baseUrl.'/css/mce.css'],
            'plugins' => [
                "advlist autolink lists",
                "code ",
                "contextmenu paste"
            ],
            'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist | link image | code",


        ]
    ]); ?>
    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
