<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\About */

$this->title = $model->title;
$this->params['breadcrumbs'][] =  $model->title;
?>
<div class="about-update card">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,'members'=>$members
    ]) ?>

</div>
