<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\About */

$this->title = $model->title;
$this->params['editlink'] = ['about/update', 'id'=>$model->id];
?>
<?php $this->beginContent('@app/views/layouts/main.php'); ?>
<div class="article-clean">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-xl-8 offset-lg-2 offset-xl-2">

                    <h1><?= Html::encode($this->title) ?></h1>
                <div class="text">
                    <?= $model->lead; ?>
                    <ul class="team team-view">
                        <?php
                        foreach ($members as $member): ?>
                            <li>
                                <div class="photo">
                                    <?php
                                    if(isset($member->img_url)){
                                       echo Html::img($member->img_url);
                                    }else{
                                        echo '<div></div>';
                                    }
                                    ?>
                                </div>
                                <div class="caption">

                                    <p class="name">
                                        <?php echo $member->name; ?>
                                    </p>
                                    <p class="role">
                                        <?php echo $member->role; ?>

                                    </p>
                                </div>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                    <?= $model->content; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->endContent(); ?>

