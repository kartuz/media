<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PageBlock */
/* @var $menu app\models\MenuItem */

$this->title = 'Добавление блока на страницу ';
?>
<div class="card">

    <h1><?= Html::encode($this->title) ?><?= Html::a($menu->title, ['/page/update/'.$menu->id])?></h1>

    <?= $this->render('_form', [
        'model' => $model, 'menu' => $menu
    ]) ?>

</div>
