<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $blocks app\models\PageBlock[] */
?>
<?php foreach ($blocks as $index => $item) :
    $class = ($index % 2 == 0) ? 'left' : 'right';
    $textClass = '';
    ?>
    <div class="article-content <?= $class ?>">
        <?php if (!empty($item->img_url)): ?>
            <div class="img-column">
                <div class="img">
                    <?= Html::img($item->img_url) ?>
                </div>
            </div>

        <?php else:
            $textClass = 'wide';
        endif; ?>
        <div class="text <?= $textClass ?>">
            <?= $item->content ?>
        </div>
    </div>
<?php endforeach; ?>
