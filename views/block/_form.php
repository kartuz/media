<?php

use dosamigos\tinymce\TinyMce;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PageBlock */
/* @var $menu app\models\MenuItem */
/* @var $form yii\widgets\ActiveForm */
?>


<?php $form = ActiveForm::begin(); ?>
<?= $form->field($model,'menu_item_id')->hiddenInput(['value'=>$menu->id])->label(false);?>
<div class="form-group">
    <?= $form->field($model, 'img_url')->hiddenInput(['maxlength' => true])->label(false); ?>
    <label>Изображение</label>
    <div class="article-photo photo" id="photoMain">
        <?php
        $h = 0;
        if (!empty($model->img_url)) {
            echo Html::img($model->img_url, ['id' => "img_url"]);
            $h = 1;
        } ?>
        <div class="inner-dot">
            <a id="imageUploadMain" href="javascript:;">Загрузить</a>
            <img src="" id="imgMain" style="display: none"/>
            <?= \troy\ImageUpload\ImageUpload::widget(
                [
                    'targetId' => 'imageUploadMain',//html dom id
                    'config' => [

                        'name' => 'file',
                        'action' => Yii::$app->getUrlManager()->createUrl(['file/upload']),

                        'onComplete' => new JsExpression("function(fileName, responseJSON){
                console.log(responseJSON);
                console.log(fileName);
                var json = $.parseJSON(responseJSON);
                
                 $('#imgMain').attr('src', '/'+json.store);
                 $('#pageblock-img_url').val('/'+json.store);
                 $('#imgMain').show();
                 $('#imageUploadMain').hide();
                 
                 }")
                    ]
                ]
            ); ?>

        </div>
        <div class="delete-hover">
            <?= Html::a('<span class="glyphicon glyphicon-trash"></span>', null, ['class' => 'delete', 'title' => 'Удалить изображение']); ?>
        </div>
        <?php
        $this->registerJs(
            "var hideloadMain = $h;
                if(hideloadMain){
                    $('#photoMain .inner-dot').hide();
                    $('#photoMain .delete-hover').show();
                }
                
                $('#photoMain .delete').on('click', function() { 
                        $('#pageblock-img_url').val('');
                        $('#photoMain .inner-dot').show();
                        $('#imageUploadMain').show();
                        $('#imgMain').attr('src', '');
                        $('#imgMain').hide();
                        $('#img_url').hide();
                        $('#photoMain .delete-hover').hide();
                    });",
            View::POS_READY,
            'my-button-handler'
        );
        ?>
    </div>
</div>
<?= $form->field($model, 'content')->widget(TinyMce::class, [
    'options' => ['rows' => 14],
    'language' => 'ru',

    'clientOptions' => [
        'content_css' => ['//' . Yii::$app->request->baseUrl . '/css/mce.css'],
        'plugins' => [
            "advlist autolink lists link charmap anchor",
            "code fullscreen",
            "contextmenu"
        ],
        'toolbar' => "undo redo | styleselect | bold italic | bullist numlist ",
        'automatic_uploads' => false,
    ]
]); ?>

<div class="form-group">
    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
</div>

<?php ActiveForm::end(); ?>


