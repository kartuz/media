<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MdConfig */

$this->title = 'Изменение параметра конфигурации';

?>
<div class="md-config-update card">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
