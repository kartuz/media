<?php

use kartik\icons\Icon;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Настройки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="md-config-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['attribute' => 'name',
                'value' => function ($model) {
                    return $model->editUrl();
                },
                'format' => 'raw'
            ],
            'value',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}',
                'visible' => true,
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a(Icon::show('pencil'), $url, [
                            'title' => Yii::t('app', 'редактировать'),
                            'class' => "btn btn-link edit"
                        ]);
                    }
                ],
            ],
        ],
    ]); ?>

</div>
