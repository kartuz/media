<?php

use dosamigos\tinymce\TinyMce;
use kartik\icons\Icon;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CarouselGroup */
/* @var $isMain integer */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin(); ?>
<?php if ($isMain == 0) {
    echo $form->field($model, 'header')->textInput(['maxlength' => true]);
    echo $form->field($model, 'content')->widget(TinyMce::class, [
        'options' => ['rows' => 14],
        'language' => 'ru',

        'clientOptions' => [
            'content_css' => ['//' . Yii::$app->request->baseUrl . '/css/mce.css'],
            'plugins' => [
                "advlist autolink lists link charmap anchor",
                "code fullscreen",
                "contextmenu"
            ],
            'toolbar' => "undo redo | styleselect | bold italic | bullist numlist ",
            'automatic_uploads' => false,
        ]
    ]);
    echo $form->field($model, 'warranty')->widget(TinyMce::class, [
        'options' => ['rows' => 1],
        'language' => 'ru',
        'clientOptions' => [
            'content_css' => ['//' . Yii::$app->request->baseUrl . '/css/mce.css'],
            'plugins' => [
                "advlist autolink lists link charmap anchor",
                "code fullscreen",
                "contextmenu"
            ],
            'toolbar' => "undo redo | styleselect | bold italic | bullist numlist ",
            'automatic_uploads' => false,
        ]
    ]);;
} ?>
<?= $form->field($model, 'main')->hiddenInput(['value' => $isMain])->label(false) ?>

<div class="form-group">
    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
</div>
<?php ActiveForm::end(); ?>
<?php if (!$model->isNewRecord): ?>
    <div class="card">
        <h2>Слайды</h2>
        <ol class="team">
            <?php

            if (!empty($model->slides)) {
                foreach ($model->slides as $slide) {
                    echo '<li><div class="photo">
                                <div class="img">
                                    <img class="" src="' . $slide->img_url . '">
                                </div>'; ?>
                    <div class="delete-hover">

                        <?=

                        Html::a(Icon::show('trashcan'), '/slide/delete/' . $model->id, [
                            'title' => 'Удалить слайд',
                            'class' => " delete",
                            'data' => [
                                'confirm' => 'Вы уверены что хотите удалить слайд? Все данные будут потеряны',
                                'method' => 'post'
                            ]]) ?>
                        <?=
                        Html::a(Icon::show('pencil'), ['/slide/update/' . $model->id, 'back' => Yii::$app->request->url], [
                            'title' => 'Редактировать слайд',
                            'class' => "edit"]) ?>

                    </div>

                    <?php echo '</div>';
                    if ($isMain == 1) {
                        echo '<div class="text">
                            <h4>' . $slide->header . '</h4>
                                ' . $slide->content . '
                        </div>';
                    }
                    echo '</li>';
                }
            }

            ?>
            <li>
                <div class="photo">
                    <div class="inner-dot">
                        <?= Html::a("Добавить", ['/slide/create', 'back' => Yii::$app->request->url, 'groupId' => $model->id]); ?>
                    </div>
                </div>
            </li>
        </ol>
    </div>
<?php endif; ?>



