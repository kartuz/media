<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CarouselGroup */

$this->title = ($model->main) ? 'Редактировать главную карусель ' : 'Редактировать карусель ' . $model->header;
$this->params['breadcrumbs'][] = ['label' => 'Carousel Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="carousel-group-update card">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model, 'isMain' => $model->main
    ]) ?>

</div>
