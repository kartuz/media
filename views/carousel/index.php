<?php

use app\models\CarouselGroup;
use kartik\icons\Icon;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $main CarouselGroup */


$this->title = 'Блоки каруселей на главной';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <h1>Основная карусель</h1>
    <?php if (isset($main)) {
        echo $this->render('/site/_carousel', [
            'model' => $main,
        ]);

        echo Html::a('Редактировать', ['update', 'id' => $main->id], ['class' => 'btn btn-success']);
    } else { ?>
        <p>
            <?= Html::a('Добавить основную карусель', ['create', 'type' => 'main'], ['class' => 'btn btn-success']) ?>
        </p>
        <?php
    }

    ?>
</div>
<div class="card">


    <h1>Остальные</h1>
    <p>
        <?= Html::a('Добавить блок', ['create', 'type' => 'common'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'header',
            'content:raw',
            'ord',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}',
                'visible' => true,
                'buttons' => [

                    'update' => function ($url, $model) {
                        return Html::a(Icon::show('pencil'), $url, [
                            'title' => Yii::t('app', 'редактировать'),
                            'class' => "btn btn-link edit"
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a(Icon::show('trashcan'), $url, [
                            'title' => Yii::t('app', ' удалить'),
                            'class' => "btn btn-link delete",
                            'data' => [
                                'confirm' => 'Вы уверены что хотите удалить? Все данные будут потеряны',
                                'method' => 'post',
                            ],
                        ]);
                    }

                ],
            ],
        ],
    ]); ?>

</div>
