<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CarouselGroup */

$this->title = 'Добавить курусель';
$this->params['breadcrumbs'][] = ['label' => 'Carousel Groups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="carousel-group-create card">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model, 'isMain' => $type
    ]) ?>

</div>
