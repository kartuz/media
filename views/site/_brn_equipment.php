<?php
/**
 * Created by PhpStorm.
 * User: pavel.chermyanin
 * Date: 22/08/2018
 * Time: 0:31
 */
?>

<section class="bnrs bnr-block">
        <h2>Поставляемое оборудование</h2>
        <ul>
            <li>
                <a href="" class="card">
                    <div class="img" style="background-image: url('img/card-img.png');">

                    </div>
                    <div class="text">

                        <h4>Светодиодные модули </h4>
                        <p>В производстве используются светодиоды DIP и SMD производителей Nation Star и Epistar. По
                            заказу
                            устанавливаем светодиоды японской компании Nichia.</p>
                        <p>Каждый модуль имеет влагозащиту IP68.</p>
                    </div>
                </a>
            </li>
            <li>
                <a href="" class="card">
                    <div class="img" style="background-image: url('img/card-img.png');">

                    </div>
                    <div class="text">

                        <h4>Светодиодные модули </h4>
                        <p>В производстве используются светодиоды DIP и SMD производителей Nation Star и Epistar. По
                            заказу
                            устанавливаем светодиоды японской компании Nichia.</p>
                        <p>Каждый модуль имеет влагозащиту IP68.</p>
                    </div>
                </a>
            </li>
            <li>
                <a href="" class="card">
                    <div class="img" style="background-image: url('img/card-img.png');">

                    </div>
                    <div class="text">

                        <h4>Светодиодные модули </h4>
                        <p>В производстве используются светодиоды DIP и SMD производителей Nation Star и Epistar. По
                            заказу
                            устанавливаем светодиоды японской компании Nichia.</p>
                        <p>Каждый модуль имеет влагозащиту IP68.</p>
                    </div>
                </a>
            </li>
            <li>
                <a href="" class="card">
                    <div class="img" style="background-image: url('img/card-img.png');">

                    </div>
                    <div class="text">

                        <h4>Светодиодные модули </h4>
                        <p>В производстве используются светодиоды DIP и SMD производителей Nation Star и Epistar. По
                            заказу
                            устанавливаем светодиоды японской компании Nichia.</p>
                        <p>Каждый модуль имеет влагозащиту IP68.</p>
                    </div>
                </a>
            </li>
        </ul>
    </section>