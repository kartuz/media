<?php
/**
 * Created by PhpStorm.
 * User: pavel.chermyanin
 * Date: 22/08/2018
 * Time: 0:40
 */
?>


<div class="right-column">
    <form class="calculator" name="calculator">
        <h4>Расчет стоимости</h4>
        <input type="hidden" name="step" class="step"/>
        <input type="hidden" name="type" class="type"/>
        <input type="hidden" name="price" class="price"/>
        <div class="slider-group">
            <p class="info">
                <label for="height">Высота экрана</label>
            <span class="selected-value">
                        <span id="widthInfo"></span>
                    <span class="unit"></span>
                </span>
            </p>
            <!--<div class="slider" data-min="0" data-max="" data-step="" data-name="width"></div>-->
            <input class="slider" name="width" id="widthSlider" data-info-field="widthInfo"
                   data-slider-id='ex1Slider' type="text" data-slider-min="0"
                   data-slider-max="20" data-slider-step="1" data-slider-value="14"/>
        </div>
        <div class="slider-group">
            <p class="info">
                <label for="height">Высота экрана</label>
            <span class="selected-value">
                        <span id="heightInfo"></span>
                    <span class="unit"></span>
                </span>
            </p>
            <input class="slider" name="height" id="heightSlider" data-info-field="heightInfo"
                   data-slider-id='ex2Slider' type="text" data-slider-min="0"
                   data-slider-max="20" data-slider-step="1" data-slider-value="14"/>
        </div>
        <label class="bold">Шаг пикселя</label>
        <ul class="check-buttons" id="pixels">
           
        </ul>
        <label class="bold">Конструкция</label>
        <div class="check-button-imaged">
            <div>
                <ul class="check-buttons">
                    <li>
                        <button type="button" class="button button-checker active" data-img="img/img-1">
                            Реечный
                        </button>
                    </li>
<!--                    <li>-->
<!--                        <button type="button" class="button button-checker" data-img="">Сетка</button>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <button type="button" class="button button-checker" data-img="">Кластерный</button>-->
<!--                    </li>-->
<!--                    <li>-->
<!--                        <button type="button" class="button button-checker" data-img="">Прозрачный</button>-->
<!--                    </li>-->
                </ul>
            </div>
            <div class="img-for-option">
                <img id="img-for-option" src=""/>
            </div>
        </div>
        <p class="summary">
        </p>
        <input data-modal="#requestModal" class="button button-big  modal-btn" value="Заявка на точный расчет">
    </form>
    <p class="note clr-gray">Цена приблизительная. Отправьте заявку на точный расчет, мы вам перезвоним, сообщим
        стоимость и поможем выбрать подходящий экран.</p>
</div>
