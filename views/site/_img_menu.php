<?php
/* @var $this yii\web\View */
/* @var $items app\models\MenuItem[] */
use yii\helpers\Url;


$itemsHTML = '';

foreach ($items as $item) {
    $itemHtml = '<li>
                <a href="/site/' . $item->alias.'" class="card">';
    $itemHtml .= ' <div class="img" style="background-image: url(\'' . $item->icon_img_url . '\');"></div>';
    $itemHtml .= '<div class="text">
                        <h4>' . $item->title . '</h4>
                        <p>' . $item->big_title . '</p>
                    </div>';
    $itemHtml .= '</a></li>';
    $itemsHTML.=$itemHtml;
}

?>


<section class="bnrs bnrs-menu">
    <ul>
        <?= $itemsHTML?>
    </ul>
    <div class="clear"></div>
</section>