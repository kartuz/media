<?php

/* @var $this yii\web\View */
use app\models\CalculationRequest;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $main CarouselGroup */
/*@var $carousels  CarouselGroup[]*/
/*@var $menuItems MenuItem[]*/


$this->title = '3 Art: Медиафасады';
$this->params['editlink'] = ['carousel/index'];
?>


<div class="content-body colored">
    <section class="presentation">
        <h1>Светодиодные экраны и медиафасады</h1>
        <p>Проектируем, производим, доставляем и монтируем по всей России</p>
        <?php if (!empty(\app\models\MdConfig::presFile())): ?>
            <a href="<?php echo \app\models\MdConfig::presFile(); ?>" class="button button-big">
                Скачать презентацию
            </a>
        <?php endif; ?>
    </section>
    <?= $this->render('_carousel', [
        'model' => $main,
    ])

    ?>

    <?= $this->render('_img_menu', [
        'items' => $menuItems,
    ]) ?>


    <?php
    foreach ($carousels as $index => $carousel) {
        echo $this->render('_carousel', [
            'model' => $carousel, 'index' => $index
        ]);
    }
    ?>

    <section class="calculator">
        <?php $form = ActiveForm::begin(['action' => ['/request/create'], 'options' => ['id' => 'req', 'data-success' => "#successRequestModal", 'data-error' => '#errorModal', 'data-method' => "post", 'method' => 'post']]); ?>
        <div class="col-sm-4 col-xs-12">
            <h2><span class="bordered">Заявка на расчет</span></h2>
            <p>Получите расчёт проекта и консультацию специалиста абсолютно бесплатно и без каких-либо
                обязательств.</p>
        </div>
        <div class="col-sm-4 col-xs-12">
            <?= $form->field($rFrom, 'name')->textInput(['maxlength' => true]) ?>
            <?= $form->field($rFrom, 'phone')->textInput(['maxlength' => true]) ?>
            <?= $form->field($rFrom, 'email')->textInput(['maxlength' => true]) ?>
            <?= $form->field($rFrom, 'type')->dropDownList(CalculationRequest::types()); ?>
        </div>
        <div class="col-sm-4 col-xs-12">
            <div class="slider-group">
                <p class="info">
                    <label for="width">Ширина экрана</label>
                <span class="selected-value">
                    <span id="widthInfo"></span>
                    <span class="unit"></span>
                 </span>
                </p>

                <!--<div class="slider" data-min="0" data-max="" data-step="" data-name="width"></div>-->
                <input class="slider" name="CalculationRequest[width]" id="widthSlider" data-info-field="widthInfo"
                       data-slider-id='ex1Slider' type="text" data-slider-min="0"
                       data-slider-max="20" data-slider-step="1" data-slider-value="14"/>
            </div>
            <div class="slider-group">
                <p class="info">
                    <label for="height">Высота экрана</label>
            <span class="selected-value">
                        <span id="heightInfo"></span>
                    <span class="unit"></span>
                </span>
                </p>

                <input class="slider" name="CalculationRequest[height]" id="heightSlider" data-info-field="heightInfo"
                       data-slider-id='ex2Slider' type="text" data-slider-min="0"
                       data-slider-max="20" data-slider-step="1" data-slider-value="14"/>
            </div>
            <?= $form->field($rFrom, 'message')->textarea(['id' => 'message']) ?>
            <?= Html::submitButton('Отправить заявку', ['class' => 'button button-big']) ?>
        </div>

        <?php ActiveForm::end(); ?>
        <div class="clear"></div>
    </section>
    <?php
    $this->registerJs(
        "fillCalconMain('mediafacades');",
        View::POS_READY,
        'calc-fill'
    );
    ?>
    <?php
    $this->registerJs(
        " $('form#req').on('beforeSubmit', function(e){
 
    console.log($('#req').serializeArray());
   
     $.post({
       url: '/request/create', 
       dataType: 'json',
       data:$('#req').serializeArray(),
       success: function(data) {
         
          $($(e.target).data('success')).modal('show');
          console.log(data);
       },
     error: function(err){
     $($(e.target).data('error')).modal('show');
 
       }
    });
       return false;
    });    ",
        View::POS_READY,
        'form-handler'
    );
    ?>
