<?php

/* @var $this yii\web\View */
/* @var $model app\models\CarouselGroup */
/* @var $index integer */
?>
<?php

$carousel = '';
$index = (isset($index)) ? $index : 0;
$carouselID = 'carousel_' . $index;
$numberOfSlides = sizeof($model->slides);
if ($numberOfSlides == 0) {
    die;
}

$innerCarousel = ' <div class="carousel-inner">';
$i = 0;
foreach ($model->slides as $slide) {
    $active = ($i == 0) ? 'active' : '';
    $innerCarousel .= '<div class="item ' . $active . '">
                        <div class="img">
                            <img class="" src="'.$slide->img_url.'">
                        </div>';
    if (isset($slide->content)) {
        $innerCarousel .= '<div class="text">
                            <h4>'.$slide->header.'</h4>
                                '.$slide->content.'
                        </div>';
    }
    $innerCarousel .= '</div>';
    $i++;
}
$innerCarousel .= '</div>';


$carouselBlock = '<div id="' . $carouselID . '" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">';

for ($i = 0; $i < $numberOfSlides; $i++) {
    $active = ($i == 0) ? 'active' : '';
    $carouselBlock .= '<li data-target="#' . $carouselID . '" data-slide-to="' . $active . '"></li>';
}

$carouselBlock .= ' </ol>';
$carouselBlock .= $innerCarousel;
$carouselBlock .= '<a class="left carousel-control" href="#' . $carouselID . '" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#' . $carouselID . '" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>';

if ($model->main) {
    $carousel = '<section class="carousel-section main-carousel">';
    $carousel .= $carouselBlock;

} else {
    $left = ($index % 2 == 0) ? 'left' : 'right';
    $carousel = '<section class="carousel-section commented-carousel ' . $left . '">';
    $carousel .= '<div class="carousel-column">';
    if (isset($model->warranty)) {
        $carousel .= '<div class="warranty-block">
                            <div class="comment">' . $model->warranty . '
                         </div>
                    </div>';
        //$carousel .= '<div class="carousel-column">';
    }
    $carousel .= $carouselBlock;
    $carousel .= '</div>';
    $carousel .= "<div class='text'>
        <h2>$model->header</h2>
        $model->content
    </div>";

}
$carousel .= '    <div class="clear"></div>';
$carousel .= '</section>';
?>

<?php echo $carousel; ?>
