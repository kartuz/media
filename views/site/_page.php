<?php
/* @var $this yii\web\View */
use yii\web\View;

/* @var $model app\models\MenuItem */
$this->params['editlink'] = ['page/update/'.$model->id];
?>

<div class="wrapper">
    <div class="content-body colored">
        <article class="left-column">
            <h1><?= $model->title ?></h1>
            <div class="lit">
                <?= $model->lead ?>
            </div>
            <div class="img main-img">
                <?= \yii\helpers\Html::img($model->main_img_url) ?>
            </div>
            <h2><?= $model->block_title ?></h2>
            <?= $this->render('/block/index', ['blocks' => $model->blocks]); ?>

        </article>
        <?= $this->render('_calc') ?>
        <div class="clear"></div>
    </div>
    <!-- banners -->

    <?php
    $this->registerJs(
        "  fillCalc('$model->alias');",
        View::POS_READY,
        'calc-fill'
    );
    ?>
</div>