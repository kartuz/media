<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CallRequest */

$this->title = 'Запрос звонка на номер : ' . $model->phone;
?>
<div class="call-request-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
