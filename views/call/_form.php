<?php

use app\models\CallRequest;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CallRequest */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="call-request-form">

    <div class="portlet-content card">

        <ul>
            <li>
                <b>Имя</b> <span><?= $model->name ?></span>
            </li>
            <li>
                <b>Телефон</b> <span><?= $model->phone ?></span>
            </li>
            <li>
                <b>Дата заявки</b><span><?php echo (new DateTime($model->request_date))->format('H:i:s d.m') ?></span>
            </li>
            <?php if(!empty($model->update_date)):?>
            <li>
                <b>Дата последнего
                    редактирования </b><span><?php echo (new DateTime($model->update_date))->format('H:i:s d.m') ?></span>
            </li>
            <?php endif;?>
        </ul>
    </div>
    <div class="card">
        <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'comment')->textarea() ?>
        <?= $form->field($model, 'proceeded')->dropDownList(CallRequest::statuses()) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить ', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
