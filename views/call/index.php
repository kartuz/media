<?php

use app\models\CallRequest;
use kartik\icons\Icon;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CallRequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Запросы звонков';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="call-request-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'name',
            'phone',
            ['attribute' => 'request_date',
                'format' => ['date', 'php:H:i.s d.m'],
                'filter'=>false
            ],
            'comment',
            ['attribute'=>'proceeded',
                'filter'=> CallRequest::statuses(),
                'value'=>function($model){
                    return $model->getStatusLabel();
                }

            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}',
                'visible' => true,
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a(Icon::show('pencil'), $url, [
                            'title' => Yii::t('app', 'редактировать'),
                            'class' => "btn btn-link edit"
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a(Icon::show('trashcan'), $url, [
                            'title' => Yii::t('app', ' удалить'),
                            'class' => "btn btn-link delete",
                            'data' => [
                                'confirm' => 'Вы уверены что хотите удалить? Все данные будут потеряны',
                                'method' => 'post',
                            ],
                        ]);
                    }

                ],
            ]
        ],
    ]); ?>
</div>
