<?php

use app\models\CalculationRequest;
use kartik\icons\Icon;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CalculationRequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = ' Запросы на расчет';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="calculation-request-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'phone',
            'email',
            'name',
            ['attribute'=>'proceeded',
                'filter'=>CalculationRequest::statuses(),
                'value'=>function($model){
                    return $model->getStatusLabel();
                }

            ],
            ['attribute'=>'type',
                'filter'=>CalculationRequest::types(),
                'value'=>function($model){
                    return $model->getTypeLabel();
                }

            ],
            ['attribute' => 'request_date',
                'format' => ['date', 'php:H:i.s d.m'],
                'filter'=>false
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}',
                'visible' => true,
                'buttons' => [
                    'update' => function ($url, $model) {
                        return Html::a(Icon::show('pencil'), $url, [
                            'title' => Yii::t('app', 'редактировать'),
                            'class' => "btn btn-link edit"
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a(Icon::show('trashcan'), $url, [
                            'title' => Yii::t('app', ' удалить'),
                            'class' => "btn btn-link delete",
                            'data' => [
                                'confirm' => 'Вы уверены что хотите удалить? Все данные будут потеряны',
                                'method' => 'post',
                            ],
                        ]);
                    }

                ],
            ],
        ],
    ]); ?>
</div>
