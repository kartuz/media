<?php

use app\models\CalculationRequest;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CalculationRequest */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="calculation-request-form">

    <?php $form = ActiveForm::begin(); ?>


    <div class="portlet-content card">

        <ul>
            <li>
                <b>Имя</b> <span><?= $model->name ?></span>
            </li>
            <li>
                <b>Телефон</b> <span><?= $model->phone ?></span>
            </li>
            <li><b>EMail</b> <span><?= $model->email ?></span>
            </li>
            <li><b>Тиа экрана</b> <span><?= $model->typeLabel ?></span>
            </li>
            <li><b>Шаг пикселя</b> <span>P<?= $model->step?></span>
            </li>
            <li><b>Размер</b><span><?= $model->width ?> x <?= $model->height ?> </span></li>
            <li>
                <b>Сообщение</b>
                <div class="text">
                    <?= $model->message; ?>
                </div>
            </li>
            <li>
                <b>Дата заявки</b><span><?php echo (new DateTime($model->request_date))->format('H:i:s d.m') ?></span>
            </li>
            <li>
                <b>Дата
                    обновления</b><span><?php echo (new DateTime($model->update_date))->format('H:i:s d.m') ?></span>
            </li>
        </ul>
    </div>

    <div class="card">
        <?= $form->field($model, 'comment')->textarea() ?>

        <?= $form->field($model, 'proceeded')->dropDownList(CalculationRequest::statuses()) ?>
    </div>


    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
