<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CalculationRequest */

$this->title = 'Запрос на расчет телефон: ' . $model->phone;
?>
<div class="calculation-request-update card">

    <h1><?= Html::encode($this->title) ?> <span class="status"><?= $model->statusLabel?></span></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
