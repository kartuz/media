<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MenuItem */

$this->title = 'Добавить страницу';
$this->params['breadcrumbs'][] = ['label' => 'Articles', 'url' => ['admin']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-create card">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
