<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\grid\GridView;
use kartik\icons\Icon;

Icon::map($this);
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Страницы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-index">

    <h1><?= Html::encode($this->title); ?> <?= Html::a(Icon::show('plus'), ['create'], ['class' => 'btn btn-outline-success']) ?> </h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'title',
            'big_title',
            'lead',
            [
                'format'=>"image", // or other formatter
                'attribute' => 'icon_img_url',
                'contentOptions' => ['class' => 'img-preview-small']
            ],
            [
                'format'=>"image", // or other formatter
                'attribute' => 'main_img_url',
                'contentOptions' => ['class' => 'img-preview']
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}',
                'visible' => true,
                'buttons' => [

                    'update' => function ($url, $model) {
                        return Html::a(Icon::show('pencil'), $url, [
                            'title' => Yii::t('app', 'редактировать'),
                            'class' => "btn btn-link edit"
                        ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a(Icon::show('trashcan'), $url, [
                            'title' => Yii::t('app', ' удалить'),
                            'class' => "btn btn-link delete",
                            'data' => [
                            'confirm' => 'Вы уверены что хотите удалить? Все данные будут потеряны',
                            'method' => 'post',
                        ],
                        ]);
                    }

                ],
            ],
        ],
    ]); ?>
</div>
