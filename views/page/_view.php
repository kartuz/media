<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MenuItem */

?>



<div class="intro">
    <h1><?= Html::a($model->title, ['/article/view', 'id' => $model->id]) ?></h1>
    <p class="lit"><?= $model->lead ?></p>
    <?= isset($model->main_img_url)?Html::img($model->main_img_url):'<div></div>' ?>

    <p class="info"><span class="date"><?= $model->published ?></span><span class="author"><?= $model->author ?></span>
    </p>
</div>


