<?php

use yii\helpers\Html;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->params['editlink'] = ['article/admin'];
?>
<?php $this->beginContent('@app/views/layouts/main.php'); ?>

    <div class="article-clean list">
        <div class="container">
            <div class="row">

                    <h1><?= Html::encode($this->title) ?></h1>


                    <?= ListView::widget([
                        'dataProvider' => $dataProvider,
                        'layout' => "{items}\n{pager}",
                        'options'=>['tag'=>'div', 'class'=>'col-lg-8 col-xl-8 offset-lg-2 offset-xl-2'],
                        'itemView' => '_view',
                        'pager' => [

                            'prevPageLabel' => '&larr; Новые',
                            'nextPageLabel' => 'Предыдущие &rarr;',
                        ],
                    ]) ?>
            </div>
        </div>
    </div>


<?php $this->endContent(); ?>