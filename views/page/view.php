<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MenuItem */

$this->title = $model->title;

$this->params['editlink'] = ['article/update', 'id'=>$model->id];

$og='';
$og=$og.'<meta property="og:title" content="'.$model->title.'"/>';
$og=$og.'<meta property="og:image" content="'.Yii::getAlias('@web').$model->main_img_url.'"/>';
$og=$og.'<meta property="og:image:width" content="200"/>';

$og=$og.'<meta property="og:type" content="article"/>';
$og=$og.'<meta property="og:url" content="'.Yii::$app->request->absoluteUrl.'"/>';
$og=$og.'<meta property="article:author" content="'.$model->author.'"/>';

$this->params['og']=$og;

?>
<?php $this->beginContent('@app/views/layouts/main.php'); ?>
<article class="article-clean">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-xl-8 offset-lg-2 offset-xl-2">
                <div class="intro">
                    <h1><?= Html::encode($this->title) ?></h1>
                    <div class="lit"><?php echo $model->lead; ?>
                    </div>
                    <div class="article-photo photo">
                        <?= Html::img($model->main_img_url, ["class"=>"img-fluid"]); ?>
                    </div>
                </div>
                <div class="text">
                    <?php echo $model->content; ?>
                    <div class="likely likely-big" data-url="<?= Yii::$app->request->absoluteUrl?>">
                        <div class="twitter"></div>
                        <div class="facebook"></div>
                        <div class="vkontakte"></div>
                        <div class="telegram"></div>
                        <div class="odnoklassniki"></div>
                    </div>

                    <p class="info"><span class="date"> 
                            <?=
                            \app\common\OutputFormatter::publishedDate($model->published);

                            ?></span><span
                            class="author"><?= Html::encode($model->author); ?></span></p>
                </div>
            </div>
        </div>
    </div>
</article>
<?php $this->endContent(); ?>

