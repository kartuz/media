<?php

use dosamigos\tinymce\TinyMce;
use kartik\icons\Icon;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\MenuItem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="article-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'big_title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'icon_img_url')->hiddenInput(['maxlength' => true])->label(false); ?>

    <div class="form-group">
        <label>Иконка для меню на главной</label>
        <div class="photo photo-square-small" id="photo">
            <?php
            $hi = 0;
            if (!empty($model->icon_img_url)) :
                echo Html::img($model->icon_img_url, ['id' => "icon_img_url"]);
                $hi = 1;
            endif; ?>
            <div class="inner-dot">
                <a id="imageUpload" href="javascript:;">Загрузить</a>
                <img src="" id="img" style="display: none"/>
                <?= \troy\ImageUpload\ImageUpload::widget(
                    [
                        'targetId' => 'imageUpload',//html dom id
                        'config' => [
                            'name' => 'file',
                            'action' => Yii::$app->getUrlManager()->createUrl(['file/upload']),
                            'onComplete' => new JsExpression("function(fileName, responseJSON){
                var json = $.parseJSON(responseJSON);
                 $('#img').attr('src', '/'+json.store);
                 $('#page-icon_img_url').val('/'+json.store);
                 $('#img').show();
                 $('#imageUpload').hide();
                 
                 }")
                        ]
                    ]
                ); ?>

            </div>
            <div class="delete-hover">
                <?= Html::a('<span class="glyphicon glyphicon-trash"></span>', null, ['class' => 'delete', 'title' => 'Удалить изображение']); ?>

            </div>
            <?php
            $this->registerJs(
                "var hideloadi = " . $hi . "
                if(hideloadi){
                    $('#photo .inner-dot').hide();
                    $('#photo .delete-hover').show();
                }
                
                $('#clean').on('click', function() { 
                        $('#page-icon_img_url').val('');
                        $('#photo .inner-dot').show();
                        $('#imageUpload').show();
                        $('#img').attr('src', '');
                        $('#img').hide();
                        $('#main_img').hide();
                        $('.delete-hover').hide();
                    });",
                View::POS_READY,
                'my-button-handler'
            );
            ?>
        </div>
    </div>
    <?= $form->field($model, 'main_img_url')->hiddenInput(['maxlength' => true])->label(false); ?>
    <div class="form-group">
        <label>Основное изображение</label>
        <div class="article-photo photo" id="photoMain">
            <?php
            $h = 0;

            if (!empty($model->main_img_url)) :
                echo Html::img($model->main_img_url, ['id' => "main_img"]);
                $h = 1;
            endif; ?>
            <div class="inner-dot">
                <a id="imageUploadMain" href="javascript:;">Загрузить</a>
                <img src="" id="imgMain" style="display: none"/>
                <?= \troy\ImageUpload\ImageUpload::widget(
                    [
                        'targetId' => 'imageUploadMain',//html dom id
                        'config' => [

                            'name' => 'file',
                            'action' => Yii::$app->getUrlManager()->createUrl(['file/upload']),

                            'onComplete' => new JsExpression("function(fileName, responseJSON){
                console.log(responseJSON);
                console.log(fileName);
                var json = $.parseJSON(responseJSON);
                 $('#imgMain').attr('src', '/'+json.store);
                 $('#page-main_img_url').val('/'+json.store);
                 $('#imgMain').show();
                 $('#imageUploadMain').hide();
                 
                 }")
                        ]
                    ]
                ); ?>

            </div>
            <div class="delete-hover">
                <?= Html::a('<span class="glyphicon glyphicon-trash"></span>', null, ['class' => 'delete', 'title' => 'Удалить изображение']); ?>

            </div>
            <?php
            $this->registerJs(
                "var hideloadMain = " . $h . "
                if(hideloadMain){
                    $('#photoMain .inner-dot').hide();
                    $('#photoMain .delete-hover').show();
                }
                
                $('#photoMain .delete').on('click', function() { 
                        $('#page-main_img_url').val('');
                        $('#photoMain .inner-dot').show();
                        $('#imageUploadMain').show();
                        $('#imgMain').attr('src', '');
                        $('#imgMain').hide();
                        $('#main_img').hide();
                        $('#photoMain .delete-hover').hide();
                    });",
                View::POS_READY,
                'my-button-handler-main'
            );
            ?>
        </div>
    </div>


    <?php echo $form->field($model, 'lead', ['template' => '{label}{hint}{input}'])
        ->hint('Выводиться в списке статей и между заголовком и основным изображением. Должна быть краткой и не должна содержать картинок.')
        ->widget(TinyMce::class, [
            'options' => ['rows' => 14],
            'language' => 'ru',

            'clientOptions' => [
                'content_css' => ['//' . Yii::$app->request->baseUrl . '/css/mce.css'],
                'plugins' => [
                    "advlist autolink lists link charmap anchor",
                    "code fullscreen",
                    "contextmenu"
                ],
                'toolbar' => "undo redo | styleselect | bold italic | bullist numlist ",
                'automatic_uploads' => false,
            ]
        ]); ?>

    <?=
    $form->field($model, 'block_title')->textInput();
    ?>


    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <?php if (!$model->isNewRecord): ?>
        <div class="card">
            <h1>Блоки на
                странице <?= Html::a(Icon::show('plus'), ['/block/create', 'back' => Yii::$app->request->url, 'menu_id' => $model->id], ['class' => 'btn btn-outline-success']) ?> </h1>
            <?php foreach ($model->blocks as $index => $item) :
                $class = ($index % 2 == 0) ? 'left' : 'right';
                $textClass = '';
                ?>
                <div class="article-content <?= $class ?>">
                    <?php if (!empty($item->img_url)): ?>
                        <div class="img-column">
                            <div class="img">
                                <?= Html::img($item->img_url) ?>
                            </div>
                        </div>

                    <?php else:
                        $textClass = 'wide';
                    endif; ?>
                    <div class="text <?= $textClass?>">
                        <?= $item->content ?>
                    </div>
                    <div class="delete-hover">
                        <?= Html::a(Icon::show('pencil'), ['/block/delete', 'id' => $item->id], ['class' => 'delete', 'title' => 'Удалить блок']); ?>
                        <?= Html::a(Icon::show('trashcan'), ['/block/update', 'id' => $item->id, 'back' => Yii::$app->request->url], ['class' => 'update', 'title' => 'Редактировать']); ?>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
</div>
