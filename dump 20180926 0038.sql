-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	8.0.12


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema media
--

CREATE DATABASE IF NOT EXISTS media;
USE media;

--
-- Definition of table `md_calculation_requests`
--

DROP TABLE IF EXISTS `md_calculation_requests`;
CREATE TABLE `md_calculation_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(70) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `phone` varchar(13) COLLATE utf8mb4_general_ci NOT NULL,
  `email` varchar(254) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `type` varchar(70) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `message` text COLLATE utf8mb4_general_ci,
  `request_date` datetime DEFAULT NULL,
  `proceeded` tinyint(4) DEFAULT NULL,
  `comment` text COLLATE utf8mb4_general_ci,
  `update_date` datetime DEFAULT NULL,
  `step` varchar(20) COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `md_calculation_requests`
--

/*!40000 ALTER TABLE `md_calculation_requests` DISABLE KEYS */;
INSERT INTO `md_calculation_requests` (`id`,`name`,`phone`,`email`,`type`,`width`,`height`,`message`,`request_date`,`proceeded`,`comment`,`update_date`,`step`) VALUES 
 (36,'Pavel Chermyanin','+79631210811','p.chermyanin@gmail.com','3',360,30,NULL,'2018-09-18 14:03:36',0,NULL,NULL,'4');
/*!40000 ALTER TABLE `md_calculation_requests` ENABLE KEYS */;


--
-- Definition of table `md_call_requests`
--

DROP TABLE IF EXISTS `md_call_requests`;
CREATE TABLE `md_call_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(70) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `phone` varchar(13) COLLATE utf8mb4_general_ci NOT NULL,
  `request_date` datetime DEFAULT NULL,
  `proceeded` tinyint(4) DEFAULT NULL,
  `comment` text COLLATE utf8mb4_general_ci,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `phone` (`phone`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `md_call_requests`
--

/*!40000 ALTER TABLE `md_call_requests` DISABLE KEYS */;
INSERT INTO `md_call_requests` (`id`,`name`,`phone`,`request_date`,`proceeded`,`comment`,`update_date`) VALUES 
 (53,'','4564564564','2018-09-17 15:20:23',0,NULL,NULL);
/*!40000 ALTER TABLE `md_call_requests` ENABLE KEYS */;


--
-- Definition of table `md_carousel_blocks`
--

DROP TABLE IF EXISTS `md_carousel_blocks`;
CREATE TABLE `md_carousel_blocks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ord` int(11) NOT NULL,
  `img_url` varchar(254) DEFAULT NULL,
  `header` varchar(100) DEFAULT NULL,
  `content` text,
  `group_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`),
  CONSTRAINT `md_carousel_blocks_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `md_carousel_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `md_carousel_blocks`
--

/*!40000 ALTER TABLE `md_carousel_blocks` DISABLE KEYS */;
INSERT INTO `md_carousel_blocks` (`id`,`ord`,`img_url`,`header`,`content`,`group_id`) VALUES 
 (1,1,'/data/ex-1.jpg','Заголовок слайда','<p>Тескт слайда</p>\r\n<p>Уточнение о качестве работ</p>',1),
 (2,1,'/data/type1.jpg',NULL,NULL,2),
 (3,2,'/data/ex-1.jpg',NULL,NULL,1);
/*!40000 ALTER TABLE `md_carousel_blocks` ENABLE KEYS */;


--
-- Definition of table `md_carousel_groups`
--

DROP TABLE IF EXISTS `md_carousel_groups`;
CREATE TABLE `md_carousel_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ord` int(11) NOT NULL,
  `header` varchar(100) DEFAULT NULL,
  `content` text,
  `main` tinyint(1) DEFAULT NULL,
  `warranty` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `md_carousel_groups`
--

/*!40000 ALTER TABLE `md_carousel_groups` DISABLE KEYS */;
INSERT INTO `md_carousel_groups` (`id`,`ord`,`header`,`content`,`main`,`warranty`) VALUES 
 (1,0,NULL,NULL,1,NULL),
 (2,1,'Производим','<p>У нас свои цеха и оборудование, поэтому мы не зависим от третьих лиц и гарантируем качество и следуем срокам.</p>\r\n<p>Цех металлообработки 327 м2 с высотой потолка 6 метров, оборудованием по резке, гибке, обработке и окраске металла, сварочные посты и кран-балки позволяют изготавливать несущие конструкции любого размера и сложности.</p>\r\n<p>В цехе сборки 250м.кв., установлены столы сборки и тестирования экранов на заявленные характеристики.</p>',0,'<p>5 лет гарантия на несущий каркас</p>\r\n<p>2 года на экраны</p>');
/*!40000 ALTER TABLE `md_carousel_groups` ENABLE KEYS */;


--
-- Definition of table `md_config`
--

DROP TABLE IF EXISTS `md_config`;
CREATE TABLE `md_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `value` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `md_config`
--

/*!40000 ALTER TABLE `md_config` DISABLE KEYS */;
INSERT INTO `md_config` (`id`,`name`,`value`) VALUES 
 (1,'site_admin','admin@media.ru'),
 (2,'from_mail','p.chermyanin@gmail.com'),
 (3,'calculation_request_mail','p.chermyanin@gmail.com'),
 (4,'call_request_mail','p.chermyanin@gmail.com'),
 (7,'presentation_file',NULL);
/*!40000 ALTER TABLE `md_config` ENABLE KEYS */;


--
-- Definition of table `md_menu_items`
--

DROP TABLE IF EXISTS `md_menu_items`;
CREATE TABLE `md_menu_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `big_title` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `main_img_url` varchar(254) COLLATE utf8mb4_general_ci NOT NULL,
  `icon_img_url` varchar(254) COLLATE utf8mb4_general_ci NOT NULL,
  `lead` text COLLATE utf8mb4_general_ci NOT NULL,
  `snippet` varchar(254) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `alias` varchar(200) COLLATE utf8mb4_general_ci NOT NULL,
  `block_title` varchar(254) COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `md_menu_items`
--

/*!40000 ALTER TABLE `md_menu_items` DISABLE KEYS */;
INSERT INTO `md_menu_items` (`id`,`title`,`big_title`,`main_img_url`,`icon_img_url`,`lead`,`snippet`,`alias`,`block_title`) VALUES 
 (1,'Медиафасады','Оптимальны для просмотра изображения с больших расстояний','/img/ex-1.jpg','/img/icn-facade.svg','Светодиодный экран представляет собой конструкцию, собранную из кабинетов, в каждом из которых установлено по 9-12 светодиодных модулей. Благодаря модульной конструкции светодиодный экран может быть любого размера.','','mediafacades','Типы медиафасадов'),
 (2,'Уличные экраны','Влагозащищенные экраны для фасадов, крыши зданий и отдельностоящие конструкции','','/img/icn-outdoor-scr.svg','<p>Светодиодные экраны для улицы обладают отличной водонепроницаемостью (IP 65/67), небольшим весом и удобной модульной конструкцией, состоящей из кабинетов прямоугольной или квадратной формы, которая позволяет создавать цельные конструкции экранов любого интересующего размера.</p>','','outdoorscreens','Варианты установки'),
 (3,'Экраны для помещений','Высокая плотность пикселей, четкое изображение даже при просмотре с малого расстояния','','/img/icn-indoor-scr.svg','Светодиодные экраны для улицы обладают отличной водонепроницаемостью (IP 65/67), небольшим весом и удобной модульной конструкцией, состоящей из кабинетов прямоугольной или квадратной формы, которая позволяет создавать цельные конструкции экранов любого интересующего размера.','','indoorscreens','Сферы применения');
/*!40000 ALTER TABLE `md_menu_items` ENABLE KEYS */;


--
-- Definition of table `md_page_blocks`
--

DROP TABLE IF EXISTS `md_page_blocks`;
CREATE TABLE `md_page_blocks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `img_url` varchar(254) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_general_ci NOT NULL,
  `menu_item_id` int(11) DEFAULT NULL,
  `ord` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `menu_item_id` (`menu_item_id`),
  CONSTRAINT `md_page_blocks_ibfk_1` FOREIGN KEY (`menu_item_id`) REFERENCES `md_menu_items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `md_page_blocks`
--

/*!40000 ALTER TABLE `md_page_blocks` DISABLE KEYS */;
INSERT INTO `md_page_blocks` (`id`,`img_url`,`content`,`menu_item_id`,`ord`) VALUES 
 (1,'/data/ex-1.jpg','<p>Монтаж экрана производится на металлические направляющие, закрепленные на фасаде здания.</p>\r\n<p>Для эстетики внешнего вида используют экраны с лицевым обслуживанием, которые устанавливаются максимально близко к стене дома. Экран с тыльным обслуживанием удобнее и дешевле обслуживать. При проектировании необходимо это учитывать и затратить чуть больше средств на изготовление и монтаж несущего металлического каркаса.&nbsp;</p>',2,2),
 (2,'','<h2>Сетка</h2>\r\n<p>Представляет собой сетку из проводов с установленными в местах пересечений светодиодами, каждый из которых работает автономно.</p>\r\n<p>Максимальное светопропускание и минимальный вес позволяет устанавливать его на полностью застекленные фасады. Гибкость дает возможность устанавливать на сложные архитектурные формы. Большой шаг между светодиодами предполагает установку на большой площади с хорошей видимостью издалека.</p>',2,1);
/*!40000 ALTER TABLE `md_page_blocks` ENABLE KEYS */;


--
-- Definition of table `md_users`
--

DROP TABLE IF EXISTS `md_users`;
CREATE TABLE `md_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(70) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `password` varchar(32) COLLATE utf8mb4_general_ci NOT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `authKey` varchar(70) COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `md_users`
--

/*!40000 ALTER TABLE `md_users` DISABLE KEYS */;
INSERT INTO `md_users` (`id`,`name`,`email`,`password`,`active`,`authKey`) VALUES 
 (1,'dev','p.chermyanin@gmail.com','a891d04caa0d66312ef8b5dce78d0800',1,'7F79EA20F4916F9072DCFC62D70B5CE4');
/*!40000 ALTER TABLE `md_users` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
