/**
 * Created by pavel.chermyanin on 13/08/2018.
 */
var calcSettings = {
    "mediafacades": {
        "maxHeight": 30,
        "maxWidth": 50,
        "mUnit": "M",
        "types": {
            "10": {
                "width": 0.96,
                "heigth": 0.96,
                "type": "реечный",
                "img": "",
                "price": 105000
            },
            "16": {
                "width": 1.024,
                "heigth": 1.024,
                "type": "реечный",
                "img": "",
                "price": 78000
            },
            "31,25": {
                "width": 1,
                "heigth": 1,
                "type": "реечный",
                "img": "",
                "price": 67000
            }
        }
    },
    "outdoorscreens": {
        "maxHeight": 960,
        "maxWidth": 1440,
        "mUnit": "CM",
        "types": {
            "3,9": {
                "width": 50,
                "heigth": 100,
                "type": "реечный",
                "img": "",
                "price": 323000
            },

            "5": {
                "width": 80,
                "heigth": 80,
                "type": "реечный",
                "img": "",
                "price": 203000
            },

            "6,4": {
                "width": 76.8,
                "heigth": 76.8,
                "type": "реечный",
                "img": "",
                "price": 172000
            },
            "8": {
                "width": 102.4,
                "heigth": 102.4,
                "type": "реечный",
                "img": "",
                "price": 120000
            },
            "10": {
                "width": 128,
                "heigth": 96,
                "type": "реечный",
                "img": "",
                "price": 97000
            }
        }
    },
    "indoorscreens": {
        "maxHeight": 800,
        "maxWidth": 800,
        "mUnit": "CM",
        "types": {
            "2,5": {
                "width": 60,
                "heigth": 30,
                "type": "реечный",
                "img": "",
                "price": 340000
            },

            "3,9": {
                "width": 50,
                "heigth": 100,
                "type": "реечный",
                "img": "",
                "price": 163000
            },
            "3,125": {
                "width": 120,
                "heigth": 30,
                "type": "реечный",
                "img": "",
                "price": 225000
            },
            "4": {
                "width": 120,
                "heigth": 30,
                "type": "реечный",
                "img": "",
                "price": 184000
            },
            "4,81": {
                "width": 50,
                "heigth": 100,
                "type": "реечный",
                "img": "",
                "price": 143000
            }
        }
    }
};
function keyByNumber(cType) {
    switch (cType) {

        case '2':
            return 'outdoorscreens';
        case '3':
            return 'indoorscreens';
        case '1':
        default:
            return 'mediafacades';
    }
}
function numberByKey(media) {
    var cType = 1;
    switch (media) {

        case 'outdoorscreens':
            cType = 2;
            break;
        case 'indoorscreens':
            cType = 3;
            break;
    }
    return cType;
}
function fillCalc(media) {
    var stng = calcSettings[media];
    var types = stng.types;
    var cType = numberByKey(media);
    $('form.calculator .type').val(cType);


    $('#pixels').empty();
    for (var key in types) {
        if (types.hasOwnProperty(key)) {
            var params = types[key];
            $('<li><button type="button" class="button button-checker" data-key =  "' + key + '"  data-price =  "' + params.price + '" data-height="' + params.heigth + '" data-width="' + params.width + '" id="id_' + key + '">P' + key + '</button></li>')
                .on('click', function (event) {
                    var h = $(event.target).data("height");
                    var w = $(event.target).data("width");
                    setSliders(h, w, stng.maxHeight, stng.maxWidth, stng.mUnit);
                    $('#pixels button').each(
                        function (i, e) {
                            $(e).removeClass("active");
                        }
                    );
                    $(event.target).addClass("active");
                    $('form.calculator .step').val($(event.target).data("key"));
                    $('form.calculator .price').val($(event.target).data("price"));


                    
                    calculate();
                })
                .appendTo($('#pixels'));
        }
    }
    $($('#pixels button')[0]).click();
    $('.check-button-imaged .button-checker').on('click',
        function () {
            var img = $(this)[0].dataset.img;
            if (img) {
                $("#img-for-option").attr('src', img + '.jpg');
                $("#img-for-option").attr('srcset', img + '.jpg 1x, ' + img + '@2x.jpg 2x');
            } else {
                $("#img-for-option").attr('src', '');
                $("#img-for-option").attr('srcset', '');
            }


            $('.check-button-imaged .button-checker').removeClass("active");
            $(this).addClass("active");

        });

    $("input.slider").each(
        function (key, value) {
            $(value).on('slideStop', function () {
                calculate();
            });
        }
    );
}

function onChangeTypeOnMain() {

    var val = $("#req #calculationrequest-type option:selected").val();
    console.log('v', val);
    var media = keyByNumber(val);
    fillCalconMain(media);

}

function fillCalconMain(media) {

    var stng = calcSettings[media];
    var types = stng.types;


    for (var key in types) {
        if (types.hasOwnProperty(key)) {
            var params = types[key];
            setSliders(params.heigth, params.width, stng.maxHeight, stng.maxWidth, stng.mUnit);
            break;
        }
    }
    $('.calculator #req #calculationrequest-type')
        .on('change', onChangeTypeOnMain)
}



function setSliders(h, w, maxH, maxW, unit) {
    var deltaH = maxH % h;
    var deltaW = maxW % w;
    var numW = Math.round((maxW - deltaW) / (2 * w));
    var numH = Math.round((maxH - deltaH) / (2 * h));


    var heightSlider = $("input#heightSlider").slider({
        step: h,
        max: (Math.round((maxH - deltaH) * 100) / 100),
        min: h,
        value: numH * h
    });
    var mySlider = $("input#widthSlider").slider({
        step: w,
        max: (Math.round((maxW - deltaW) * 100) / 100),
        min: w,
        value: numW * w
    });

    $("input#heightSlider").val(numH * h);
    $("input#widthSlider").val(numW * w);
    mySlider.slider("setValue", mySlider.slider("getValue"));
    heightSlider.slider("setValue", heightSlider.slider("getValue"));
    $("input.slider").each(
        function (key, value) {
            sliderHandler(value);
        }
    );
    $('.slider-group .unit').text(unit);

}


function sliderHandler(elem) {
    var value = $(elem).val();
    $('#' + $(elem)[0].dataset.infoField).text(value);
};
function calculate() {
    var form = $('form.calculator');
    var key = $('form.calculator .step').val();
    var mediaK = $('form.calculator .type').val();
    var media = keyByNumber(mediaK);
    var params = calcSettings[media];
    var chType = params.types[key];

    var numH = Math.round($("input#heightSlider").val() / chType.heigth);
    var numW = Math.round($("input#widthSlider").val() / chType.width);
    var price = numH * numW * chType.price;

    $('#calculationrequest-width').val($("input#widthSlider").val());
    $('#calculationrequest-height').val($("input#heightSlider").val());
    $('#calculationrequest-type').val(mediaK);
    $('#calculationrequest-step').val(key);

    $('form.calculator .summary').html(formatNumber(price) + " &#8381;");
};

function formatNumber(number) {
    var comma = '&nbsp;',
        string = Math.max(0, number).toFixed(0),
        length = string.length,
        end = /^\d{4,}$/.test(string) ? length % 3 : 0;
    return (end ? string.slice(0, end) + comma : '') + string.slice(end).replace(/(\d{3})(?=\d)/g, '$1' + comma);
}